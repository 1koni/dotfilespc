#!/bin/sh

#term="kitty"
#Editor=$(emacsclient -c -a 'emacs')

chosen=$(printf "Alacritty\nNewsBoat\nEmacs\nLF\nNeoVim\nPicom\nRanger\nRifle(Ranger)\nXMobar1\nXMobar2\nXMonad\nZsh\nHyprland\nWaybar" | sort | dmenu -i -p "Configs")

if [ "$chosen" = "Alacritty" ]; then
         emacsclient -c -a 'emacs' ~/.dotfilespc/.config/alacritty/alacritty.yml
elif [ "$chosen" = "NewsBoat" ]; then
				 chosen=$(printf "Config\nUrls" | dmenu -i -p "NewsBoat")
				 if [ "$chosen" = "Config" ]; then
         					emacsclient -c -a 'emacs' ~/.newsboat/config
				 elif [ "$chosen" = "Urls" ]; then
         					emacsclient -c -a 'emacs' ~/.newsboat/urls
				 fi
elif [ "$chosen" = "Emacs" ]; then
         emacsclient -c -a 'emacs' ~/.dotfiles/.emacs.d/Emacs.org	
elif [ "$chosen" = "NeoVim" ]; then
         emacsclient -c -a 'emacs' ~/.dotfiles/.config/nvim/init.vim
elif [ "$chosen" = "LF" ]; then
         emacsclient -c -a 'emacs' ~/.config/lf/lfrc
elif [ "$chosen" = "Ranger" ]; then
         emacsclient -c -a 'emacs' ~/.dotfiles/.config/ranger/rc.conf
elif [ "$chosen" = "Rifle(Ranger)" ]; then
         emacsclient -c -a 'emacs' ~/.dotfiles/.config/ranger/rifle.conf
elif [ "$chosen" = "Zsh" ]; then
         emacsclient -c -a 'emacs' ~/.dotfiles/.zshrc
elif [ "$chosen" = "Picom" ]; then
         emacsclient -c -a 'emacs' ~/.dotfilespc/.config/picom.conf
elif [ "$chosen" = "XMonad" ]; then
         emacsclient -c -a 'emacs' ~/.dotfilespc/.xmonad/xmonad.hs
elif [ "$chosen" = "XMobar1" ]; then
         emacsclient -c -a 'emacs' ~/.dotfilespc/.xmobarrc1
elif [ "$chosen" = "XMobar2" ]; then
         emacsclient -c -a 'emacs' ~/.dotfilespc/.xmobarrc2
elif [ "$chosen" = "Hyprland" ]; then
         emacsclient -c -a 'emacs' ~/.config/hypr/hyprland.conf
elif [ "$chosen" = "Waybar" ]; then
         emacsclient -c -a 'emacs' ~/.config/waybar/config
fi
