#!/bin/sh


chosen=$(printf "Alacritty\nKitty\nEmacs\nHyprland\nNeoVim\nRanger\nRifle(Ranger)\nWaybar\nXMobar\nXMonad\nZsh\nNewsBoat\nScripts\nFuzzel" | sort | fuzzel -d -l 14 -p "Configs: ")

if [ "$chosen" = "Alacritty" ]; then
	emacsclient -c -a 'emacs' ~/.dotfilespc/.config/alacritty/alacritty.yml
elif [ "$chosen" = "NewsBoat" ]; then
	chosen=$(printf "Config\nUrls" | fuzzel -d -l 2 -p "NewsBoat: ")
	if [ "$chosen" = "Config" ]; then
		emacsclient -c -a 'emacs' ~/.newsboat/config
	elif [ "$chosen" = "Urls" ]; then
		emacsclient -c -a 'emacs' ~/.newsboat/urls
	fi
elif [ "$chosen" = "Emacs" ]; then
	emacsclient -c -a 'emacs' ~/.dotfiles/.emacs.d/Emacs.org
elif [ "$chosen" = "Hyprland" ]; then
	chosen=$(printf "Hyprland\nPyprland" | fuzzel -d -l 2 -p "Hyprland: ")
	if [ "$chosen" = "Hyprland" ]; then
	    emacsclient -c -a 'emacs' ~/.dotfilespc/.config/hypr/hyprland.conf
	elif [ "$chosen" = "Pyprland" ]; then
	    emacsclient -c -a 'emacs' ~/.dotfilespc/.config/hypr/pyprland.toml
	fi
elif [ "$chosen" = "Waybar" ]; then
	chosen=$(printf "Waybar\nStyle\nRestart" | fuzzel -d -l 3 -p "Waybar: ")
	if [ "$chosen" = "Waybar" ]; then
		emacsclient -c -a 'emacs' ~/.dotfilespc/.config/waybar/config
	elif [ "$chosen" = "Style" ]; then
		emacsclient -c -a 'emacs' ~/.dotfilespc/.config/waybar/style.css
	elif [ "$chosen" = "Restart" ]; then
		killall waybar
		waybar &
	fi
elif [ "$chosen" = "Kitty" ]; then
	emacsclient -c -a 'emacs' ~/.config/kitty/kitty.conf
elif [ "$chosen" = "NeoVim" ]; then
	emacsclient	-c -a 'emacs' ~/.config/nvim/init.vim	
# elif [ "$chosen" = "Picom" ]; then
# 	emacsclient	-c -a 'emacs' ~/.config/picom.conf	
elif [ "$chosen" = "Ranger" ]; then
	emacsclient -c -a 'emacs' ~/.dotfiles/.config/ranger/rc.conf
elif [ "$chosen" = "Rifle(Ranger)" ]; then
	emacsclient -c -a 'emacs' ~/.dotfiles/.config/ranger/rifle.conf
elif [ "$chosen" = "Fuzzel" ]; then
	emacsclient -c -a 'emacs' ~/.dotfiles/.config/fuzzel/fuzzel.ini
elif [ "$chosen" = "XMobar" ]; then
	chosen=$(printf "Xmobar1\nXmobar2" | fuzzel -d -l 2 -p "Xmobar: ")
	if [ "$chosen" = "Xmobar1" ]; then
		emacsclient	-c -a 'emacs' ~/.dotfilespc/.xmobarrc1
	elif [ "$chosen" = "Xmobar2" ]; then
		emacsclient	-c -a 'emacs' ~/.dotfilespc/.xmobarrc2
	fi
elif [ "$chosen" = "XMonad" ]; then
	emacsclient	-c -a 'emacs' ~/.dotfilespc/.xmonad/xmonad.hs
elif [ "$chosen" = "Zsh" ]; then
	chosen=$(printf "Zshrc\nZshenv" | fuzzel -d -l 2 -p "Zsh: ")
	if [ "$chosen" = "Zshrc" ]; then
	    emacsclient	-c -a 'emacs' ~/.dotfiles/.zshrc
	elif [ "$chosen" = "Zshenv" ]; then
	    emacsclient	-c -a 'emacs' ~/.zshenv
	fi
elif [ "$chosen" = "Scripts" ]; then
	chosen=$(printf "Audio\nConfigs\nKill\nPower\nSearch\nHyprsunset\nBookmarks" | fuzzel -d -l 7 -p "Scripts: ")
	if [ "$chosen" = "Audio" ]; then
		emacsclient	-c -a 'emacs' ~/.dotfiles/.config/scripts/waudio.sh
	elif [ "$chosen" = "Configs" ]; then
		emacsclient	-c -a 'emacs' ~/.dotfilespc/.config/scripts/wconfigs.sh
	elif [ "$chosen" = "Kill" ]; then
		emacsclient	-c -a 'emacs' ~/.dotfiles/.config/scripts/wkill.sh
	elif [ "$chosen" = "Power" ]; then
		emacsclient	-c -a 'emacs' ~/.dotfiles/.config/scripts/wpower.sh
	elif [ "$chosen" = "Search" ]; then
		emacsclient	-c -a 'emacs' ~/.dotfiles/.config/scripts/wsearch.sh
	elif [ "$chosen" = "Hyprsunset" ]; then
		emacsclient	-c -a 'emacs' ~/.dotfiles/.config/scripts/hyprsunset.sh
	elif [ "$chosen" = "Bookmarks" ]; then
	    chosen=$(printf "Firefox\nBrave" | fuzzel -d -l 2 -p "Bookmarks: ")
	    if [ "$chosen" = "Brave" ]; then 
				emacsclient	-c -a 'emacs' ~/.dotfiles/.config/scripts/brave-bookmarks.sh
	    elif [ "$chosen" = "Firefox" ]; then
				emacsclient	-c -a 'emacs' ~/.dotfiles/.config/scripts/firefox-bookmarks.sh
	    fi
	fi
fi
