import XMonad
import XMonad.Config.Desktop ( desktopConfig )
import System.IO ( hPutStrLn )

import XMonad.Util.Run (spawnPipe)
import XMonad.Util.EZConfig(additionalKeysP)
import XMonad.Util.SpawnOnce ( spawnOnce )
import XMonad.Util.NamedScratchpad

import XMonad.Hooks.SetWMName ( setWMName )
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.StatusBar ()
import XMonad.Hooks.StatusBar.PP
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops ( ewmh, ewmhFullscreen )

import XMonad.Layout.NoBorders
import XMonad.Layout.Spacing
import XMonad.Layout.ResizableTile (MirrorResize(MirrorExpand, MirrorShrink))
import XMonad.Layout.ThreeColumns
import XMonad.Layout.LayoutModifier
import XMonad.Layout.Magnifier ( magnifiercz' )
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(FULL))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

import XMonad.Actions.UpdatePointer ( updatePointer )
import XMonad.Actions.SpawnOn 
import XMonad.Actions.EasyMotion (selectWindow, EasyMotionConfig(..), textSize)
import XMonad.Actions.CopyWindow

-- import XMonad.Operations.unGrab
import qualified XMonad.StackSet as W
import Language.Haskell.TH.Lib (rationalL)
--import Data.Colour.Names (darkgreen, lightyellow)
import XMonad.Layout.TabBarDecoration (XPPosition(Bottom))

  

myTerminal :: String
myTerminal = "kitty"

myEmacs :: String
myEmacs = "emacsclient -c -a 'emacs' "

myStartupHook :: X ()
myStartupHook = do
        setWMName "LG3D"
        -- spawnOnce "~/.screenlayout/quer.sh"
        spawnOnce "picom &"
        spawnOnce "lxsession &"
        -- spawnOnce "dropbox &"
        -- spawnOnce "internxt-drive &"
        spawnOnce "blueman-applet &"
        spawnOnce "kdeconnect-indicator &"
        spawnOnce "trayer --edge top --monitor 1 --align right --widthtype percent --width 8 --padding 6 --SetDockType true --SetPartialStrut true --expand true --transparent true --alpha 0 --tint 0x000000 --height 16 &"
        spawnOnce "redshift-gtk &"
        spawnOnce "clipmenud &"
        spawnOnce "/usr/bin/emacs --daemon &"
        spawnOnce ( myTerminal ++ " -e  xmodmap -e 'keycode 29 = y' && xmodmap -e 'keycode 52 = z'")
        spawnOnce ( myTerminal ++ " -e xmodmap -e 'clear mod4' && xmodmap -e 'keycode 134 = Control_R' && xmodmap -e 'add control = Control_R' && sleep 5 && xmonad --restart " )
        spawnOnce "setxkbmap -option caps:swapescape"
        spawnOnce "xset r rate 300 50"
        -- spawnOnce "mpv --no-video ~/Media/Music/Sounds/goblin-says-yes.mp3 &"
        spawnOnce "nitrogen --restore &"

myKeys :: [(String, X ())]
myKeys =
-- Browser  
        [("M-b b",        spawn "brave")
        ,("M-b S-b",      spawn "brave --incognito")
        ,("M-b f",        spawn "firefox")
        ,("M-b S-f",      spawn "firefox --private")
        ,("M-b l",        spawn "floorp")
        ,("M-b S-l",      spawn "floorp --private-window")
        ,("M-b v",        spawn "vivaldi-stable")
        ,("M-b S-v",      spawn "vivaldi-stable --incognito")
        ,("M-b q",        spawn "qutebrowser")
        ,("M-b t",        spawn "thunderbird")
        ,("M-b y",        spawn "freetube")
        ,("M-b n",        spawnOn "8" (myTerminal ++ " -e newsboat"))
        ,("M-b s",        spawn "spotify-launcher")
        ,("M-b g",        spawn "signal-desktop")
        ,("M-b d",        spawn "discord")
--Audio/Media
        ,("<XF86AudioPlay>",            spawn "playerctl play-pause")
        ,("<XF86AudioStop>",            spawn "playerctl stop")
        ,("<XF86AudioPrev>",            spawn "playerctl previous")
        ,("<XF86AudioNext>",            spawn "playerctl next")
        ,("<XF86AudioLowerVolume>",     spawn "pamixer -d 5")
        ,("<XF86AudioRaiseVolume>",     spawn "pamixer -i 5")
        ,("<XF86AudioMute>",            spawn "pamixer -t")
        ,("<XF86Calculator>",           spawn "rfkill block bluetooth")
        ,("<XF86Tools>",                spawn "rfkill unblock bluetooth")
--Other
        ,("M-z",          spawn ( myTerminal ++ " -e ranger "))
        ,("M-S-z",        spawn ( myTerminal ++ " -e sudo ranger "))
        ,("M-a",          spawn "thunar")
        ,("C-M1-h",       spawn "clipmenu")
        -- ,("M-C-o",        unGrab *> spawn "scrot -s")
--Dmenu/Scripts
        ,("M-p d",        spawn "dmenu_run")
        ,("M-p r",        spawn "rofi -show drun -icon-theme \"Papirus\" -show-icons")
        ,("M-p s",        spawn ".config/scripts/search.sh")
        ,("M-p S-s",      spawn ".config/scripts/search.sh -p")
        ,("M-p C-s",      spawn ".config/scripts/search.sh -n")
        ,("M-p p",        spawn ".config/scripts/power.sh")
        ,("M-p c",        spawn ".config/scripts/configs.sh")
        ,("M-p k",        spawn ".config/scripts/kill.sh")
        ,("M-p a",        spawn ".config/scripts/audio.sh")
        ,("M-p x",        spawn ".config/scripts/xmonad.sh")
--Emacs
        ,("M-c e",        spawn myEmacs)
        ,("M-c d",        spawn (myEmacs ++ "--eval '(dired nil)'"))
        ,("M-c m",        spawn (myEmacs ++ "--eval '(mu4e)'"))
        ,("M-c u",        spawn (myEmacs ++ "--eval '(list-packages)'"))
        ,("M-c a a",      spawn (myEmacs ++ "--eval '(org-agenda)'"))
        ,("M-c a t",      spawn (myEmacs ++ "~/Organization/Emacs/Calendar/Tasks.org"))
        ,("M-c a h",      spawn (myEmacs ++ "~/Organization/Emacs/Calendar/Habits.org"))
        ,("M-c j",        spawn (myEmacs ++ "~/Organization/Emacs/Calendar/Journal.org"))
--Scratchpad
        ,("M-C-<Return>",        namedScratchpadAction myScratchPads "terminal")
        ,("M-q",                 namedScratchpadAction myScratchPads "calculator")
        ,("M-c s",               namedScratchpadAction myScratchPads "emacs-scratch")
-- Window resizing
        ,("M-h", sendMessage Shrink)                   -- Shrink horiz window width
        ,("M-l", sendMessage Expand)                   -- Expand horiz window width
        ,("M-f", sendMessage (MT.Toggle FULL) >> sendMessage ToggleStruts)
-- Easy Motion
        ,("M-s", selectWindow def{txtCol="Green", cancelKey=xK_Escape, emFont="xft: Sans-40", overlayF=textSize} >>= (`whenJust` windows . W.focusWindow))
        ,("M-x", selectWindow def{txtCol="Red", cancelKey=xK_Escape, emFont="xft: Sans-40", overlayF=textSize} >>= (`whenJust` killWindow))
-- Tagging
        ,("M-C-a", windows copyToAll)   -- copy window to all workspaces
        ,("M-C-q", killAllOtherCopies)  -- kill copies of window on other workspaces
        ,("M-C-c", kill1)               -- kill only this copy, not every window 
        ]
        ++
        [ (mask ++ "M-" ++ [key], screenWorkspace scr >>= flip whenJust (windows . action))
         | (key, scr)  <- zip "er" [0,1] -- was [0..] *** change to match your screen order ***
         , (action, mask) <- [ (W.view, "") , (W.shift, "S-")]
        ]
        ++
-- DWM like tagging
        [("M-" ++ m ++ k, windows $ f i)
            | (i, k) <- zip myWorkspaces (map show [1 :: Int ..])
            , (f, m) <- [(W.greedyView, ""), (W.shift, "S-"), (copy, "M-C-")]]
    -- The following lines are needed for named scratchpads.
    -- where nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
    --       nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))


  
myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "calculator" spawnCalc findCalc manageCalc
                , NS "emacs-scratch" spawnEmacs findEmacs manageEmacs
                ]
  where
    spawnTerm  = myTerminal ++ " -T scratchpad"
    findTerm   = title =? "scratchpad"
    manageTerm = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w
    spawnCalc  = "qalculate-gtk"
    findCalc   = className =? "Qalculate-gtk"
    manageCalc = customFloating $ W.RationalRect l t w h
               where
                 h = 0.5
                 w = 0.4
                 t = 0.75 -h
                 l = 0.70 -w
    spawnEmacs  = "emacsclient -a='' -nc --frame-parameters='(quote (name . \"emacs-scratch\"))'"
    findEmacs   = title =? "emacs-scratch"
    manageEmacs = customFloating $ W.RationalRect l t w h
               where
                 h = 0.9
                 w = 0.9
                 t = 0.95 -h
                 l = 0.95 -w


myWorkspaces :: [String]
myWorkspaces = ["1","2","3","4","5","6","7","8","9"]

myManageHook = composeAll
       [ className =? "thunderbird" -->   doShift "7"
        --className =? "Emacs" -->         doShift "4"
       , className =? "KeePassXC" -->     doShift "7"
       , className =? "FreeTube" -->      doShift "8"
       , className =? "mpv" -->           doShift "9"
       -- , className =? "Qalculate-gtk" --> doFloat
       , manageDocks
       ] <+> namedScratchpadManageHook myScratchPads

myLayoutHook = avoidStruts  $ smartBorders $ spacingRaw True (Border 0 5 5 5) True (Border 5 5 5 5) True $ mkToggle(FULL ?? EOT) myDefaultLayout

myDefaultLayout = tiled ||| threecol ||| Full
    where
      threecol = magnifiercz' 1.8 $ ThreeColMid nmaster delta ratio
      tiled    = Tall nmaster delta ratio
      nmaster  = 1
      ratio    = 1/2
      delta    = 3/100


main :: IO ()
main = do
    xmproc1 <- spawnPipe "xmobar -x 0 $HOME/.xmobarrc1"
    xmproc2 <- spawnPipe "xmobar -x 1 $HOME/.xmobarrc2"
    xmonad $ ewmhFullscreen . ewmh $ docks def
        { layoutHook         = myLayoutHook
        , modMask            = mod4Mask
        , borderWidth        = 2
        , manageHook         = manageSpawn <+> myManageHook <+> manageHook def
        , logHook            = dynamicLogWithPP xmobarPP
        
                             { ppOutput = \x -> hPutStrLn xmproc1 x >> hPutStrLn xmproc2 x
                             , ppTitle = xmobarColor "green" "" . shorten 100
                             , ppCurrent = xmobarColor "lightgreen" "" . wrap
                             "<box type=Bottom width=2 mt=2 color=lightgreen >" "</box>"
                             , ppVisible = xmobarColor "lightgreen" "" 
                             , ppHidden = xmobarColor "yellow" ""
                             }
                             >> updatePointer (0.005, 0.01) (0, 0)
        , terminal           = myTerminal
        , startupHook        = myStartupHook
        , focusedBorderColor = "purple"
        , workspaces         = myWorkspaces
        } `additionalKeysP` myKeys

desktop _                    = desktopConfig
